

package com.tymofieiev.sergii.myweather;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;


import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
/**
 * Created by TSV on 11.01.2017.
 */
public class PlaceAutocompleteAdapter extends BaseAdapter implements Filterable {

private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
private Context context;
private ArrayList<AutocompletePrediction> mResultList;
private GoogleApiClient mGoogleApiClient;
private LatLngBounds mBounds;
private AutocompleteFilter mPlaceFilter;

public PlaceAutocompleteAdapter(Context context, GoogleApiClient googleApiClient,
                                LatLngBounds bounds, AutocompleteFilter filter) {
    this.context = context;
    mGoogleApiClient = googleApiClient;
    mBounds = bounds;
    //mPlaceFilter = filter;
    mPlaceFilter = new AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
            .build();
    mResultList = new ArrayList<>();
}
public void setBounds(LatLngBounds bounds) {
    mBounds = bounds;
}

@Override
public int getCount() {
    return mResultList.size();
}

@Override
public AutocompletePrediction getItem(int position) {
    return mResultList.get(position);
}

@Override
public long getItemId(int position) {
    return position;
}

@Override
public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.place_search_item, parent, false);
    }
    AutocompletePrediction item = getItem(position);
    ((TextView) convertView.findViewById(R.id.item_name)).setText(item.getPrimaryText(STYLE_BOLD));
    String secondaryText = item.getSecondaryText(STYLE_BOLD).toString();
    if(secondaryText != null && !secondaryText.isEmpty()) {
        ((TextView) convertView.findViewById(R.id.item_name_full)).setText(", "+secondaryText);
    }
    return convertView;
}

@Override
public Filter getFilter() {
    return new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            ArrayList<AutocompletePrediction> filterData = new ArrayList<>();
            // Skip the autocomplete query if no constraints are given.
            if (constraint != null) {
                // Query the autocomplete API for the (constraint) search string.
                filterData = getAutocomplete(constraint);
            }
            results.values = filterData;
            if (filterData != null) {
                results.count = filterData.size();
            } else {
                results.count = 0;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results != null && results.count > 0) {
                mResultList = (ArrayList<AutocompletePrediction>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            if (resultValue instanceof AutocompletePrediction) {
                return ((AutocompletePrediction) resultValue).getFullText(null);
            } else {
                return super.convertResultToString(resultValue);
            }
        }
    };
}


private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
    if (mGoogleApiClient.isConnected()) {
        PendingResult<AutocompletePredictionBuffer> results =
                Places.GeoDataApi
                        .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                mBounds, mPlaceFilter);
        AutocompletePredictionBuffer autocompletePredictions = results
                .await(60, TimeUnit.SECONDS);
        final Status status = autocompletePredictions.getStatus();
        if (!status.isSuccess()) {
            Toast.makeText(App.getContext(), "Error contacting API: " + status.toString(),
                    Toast.LENGTH_SHORT).show();
            autocompletePredictions.release();
            return null;
        }
        return DataBufferUtils.freezeAndClose(autocompletePredictions);
    }
    return null;
}
    public void clearList(){
        mResultList = new ArrayList<>();
        notifyDataSetChanged();
    }
}
