package com.tymofieiev.sergii.myweather;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;



/**
 * Created by TSV on 11.01.2017.
 */

public class Utils {
    public static final double DEFAULT_LAT = 50.4501786;
    public static final double DEFAULT_LNG = 30.5234798;

    public static boolean isLocationEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        return !(!gps_enabled && !network_enabled);
    }

    public static LocationRequest getLocationRequest(long locationUpdateInterval, long locationFastestInterval, float locationDisplacement) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(locationUpdateInterval);
        locationRequest.setFastestInterval(locationFastestInterval);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(locationDisplacement);
        return locationRequest;
    }

    public static String getStringById(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static String getStringById(int id) {
        return getStringById(App.getContext(), id);
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public static LatLngBounds getDefaultBounds() {
        return new LatLngBounds(new LatLng(44.90364165394314, 25.847602039575573), new LatLng(52.89643201400083, 34.44433204829693));
    }


    public static LatLng makeLatLng(String stringLat, String stringLng) {
        if (TextUtils.isEmpty(stringLat) || TextUtils.isEmpty(stringLng)) {
            return new LatLng(DEFAULT_LAT, DEFAULT_LNG);
        }
        try {
            double lat = Double.parseDouble(stringLat);
            double lng = Double.parseDouble(stringLng);
            return new LatLng(lat, lng);
        } catch (Exception e) {
            return new LatLng(DEFAULT_LAT, DEFAULT_LNG);
        }

    }
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if(activeNetwork == null){
            return false;
        }
        return activeNetwork.isConnectedOrConnecting();
    }
    public static void setMobileDataTransferEnabled(final Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName("com.android.settings",
                "com.android.settings.Settings$DataUsageSummaryActivity"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);


    }
}
