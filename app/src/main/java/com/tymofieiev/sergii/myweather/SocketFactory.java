package com.tymofieiev.sergii.myweather;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
/**
 * Created by TSV on 11.01.2017.
 */
public class SocketFactory extends SSLSocketFactory {

	private static class DefaultTrustManager implements X509TrustManager {
		@Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
		@Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException { }
		@Override
        public X509Certificate[] getAcceptedIssuers() { return null; }
	}

	static class NullHostNameVerifier implements HostnameVerifier {
	    public boolean verify(String hostname, SSLSession session) {
//	    	if(hostname.endsWith(".reteko.com") || hostname.endsWith(".fwupd.com")){

//	    		return true;
//	    	}else{
//	    		Log.e("My_log", "Hostname " + hostname + " not verified!");
//	    		return false;
//	    	}
            return false;
	    }
	}

	private SSLSocketFactory ssf;

	public SocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager() }, new SecureRandom());
		this.ssf=ctx.getSocketFactory();
	}
	
	@Override
	public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
		return ssf.createSocket(s,host,port,autoClose);
	}

	@Override
	public String[] getDefaultCipherSuites() {
		return ssf.getDefaultCipherSuites();
	}

	@Override
	public String[] getSupportedCipherSuites() {
		return ssf.getSupportedCipherSuites();
	}

	@Override
	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		return ssf.createSocket(host, port);
	}

	@Override
	public Socket createSocket(InetAddress host, int port) throws IOException {
		return ssf.createSocket(host, port);
	}

	@Override
	public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
		return ssf.createSocket(host, port, localHost, localPort);
	}

	@Override
	public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
		return ssf.createSocket(address, port, localAddress, localPort);
	}

}
