package com.tymofieiev.sergii.myweather;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TSV on 11.01.2017.
 */

public class WeatherPlace  {
    private  String placeName;
    private  String placeCode;
    private  String imageName;
    private  String temperature;

    public WeatherPlace(String data){
        placeName = getNameFromData(data,"");
        placeCode = getCodeFromData(data,"");
        imageName = getImageNameFromData(data, "");
        temperature = getTemperatureFromData(data, "");
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPlaceCode() {
        return placeCode;
    }


    private static String getNameFromData(String data, String defaultValue) {
        if(TextUtils.isEmpty(data)){
            return defaultValue;
        }
        final JSONObject jsonRoot;
        try {
            jsonRoot = new JSONObject(data);
            return safeGetJsonValue("name", jsonRoot, "").trim();
        } catch (JSONException e) {
            return defaultValue;
        }
    }
    private static String getCodeFromData(String data, String defaultValue) {
        if(TextUtils.isEmpty(data)){
            return defaultValue;
        }
        final JSONObject jsonRoot , jsonChild;
        try {
            jsonRoot = new JSONObject(data);
            jsonChild = jsonRoot.getJSONObject("sys");

           return safeGetJsonValue("country", jsonChild, "").trim();
        } catch (JSONException e) {
            return defaultValue;
        }
    }
    private static String getImageNameFromData(String data, String defaultValue) {
        if(TextUtils.isEmpty(data)){
            return defaultValue;
        }
        final JSONObject jsonRoot;
        final JSONArray jsonChild;
        try {
            jsonRoot = new JSONObject(data);
            jsonChild = jsonRoot.getJSONArray("weather");
            for (int i = 0; i < jsonChild.length(); i++) {
                if (jsonChild.isNull(i)) {
                    return defaultValue;
                }
                JSONObject element = jsonChild.getJSONObject(i);
                return safeGetJsonValue("icon", element, "").trim();
            }

        } catch (JSONException e) {
            return defaultValue;
        }
        return defaultValue;
    }


    private String getTemperatureFromData(String data, String defaultValue){
        if(TextUtils.isEmpty(data)){
            return defaultValue;
        }
        final JSONObject jsonRoot , jsonChild;
        try {
            jsonRoot = new JSONObject(data);
            jsonChild = jsonRoot.getJSONObject("main");
            return safeGetJsonValue("temp", jsonChild, "").trim();
        } catch (JSONException e) {
            return defaultValue;
        }
    }


    public static String safeGetJsonValue(String key, JSONObject jsonObject, String defaultValue) throws JSONException {
        if (jsonObject.isNull(key)) {
            return defaultValue;
        }
        return jsonObject.getString(key);
    }

    public String getImageName() {
        return Communication.DOMAIN_FRONT_IMAGE+imageName+".png";
    }

    public String getTemperature() {
        return temperature;
    }
}
