package com.tymofieiev.sergii.myweather;

import android.annotation.SuppressLint;
import android.webkit.URLUtil;



import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import com.google.android.gms.maps.model.LatLng;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by TSV on 11.01.2017.
 */

public class Communication {
    static final String DOMAIN_FRONT = "http://api.openweathermap.org/data/2.5/weather?APPID=04e03f4783d8059ba2150503c81f0376&units=metric";
    public static final String DOMAIN_FRONT_IMAGE = "http://openweathermap.org/img/w/";
    private static String cookie_front = null;
    private static Communication instance;


    public enum HTTP_CMD {
        POST("POST"),
        GET("GET"),
        PUT("PUT"),
        DELETE("DELETE");
        private String represent;

        public String toString() {
            return this.represent;
        }

        private HTTP_CMD(String represent) {
            this.represent = represent;
        }
    }

    public static Communication getInstance() {
        if (instance == null) {
            instance = new Communication();
        }
        return instance;
    }

    public void requestResponse(HTTP_CMD tHttpCmd, String tUrl, TreeMap<String, String> requestPropertyMap, String postBody, OutputStream os, ResponseListener responseListener) {
        if (!URLUtil.isNetworkUrl(tUrl)) {
            if (responseListener != null) {
                responseListener.onError("Incorrect server URL : " + tUrl);
                return;
            }
        }
        String resultString = null;

        URL url = null;
        HttpURLConnection httpConnection = null;
        try {
            url = new URL(tUrl);
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod(tHttpCmd.toString());
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("Accept-Language", Locale.getDefault().getLanguage());
            if (requestPropertyMap != null) {
                Set<String> keys = requestPropertyMap.keySet();
                for (String key : keys) {
                    if (requestPropertyMap.get(key) != null) {
                        httpConnection.setRequestProperty(key, requestPropertyMap.get(key));
                    }
                }
            }
            httpConnection.setConnectTimeout(30000);
            httpConnection.setReadTimeout(30000);
            if (cookie_front != null) {
                httpConnection.setRequestProperty("Cookie", cookie_front);
            }
            if (httpConnection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) httpConnection).setSSLSocketFactory(new SocketFactory());
            }

            httpConnection.setDoInput(true);
            if (tHttpCmd != HTTP_CMD.GET && tHttpCmd != HTTP_CMD.DELETE) {
                httpConnection.setDoOutput(true); // this operator transform GET to POST
            }
            httpConnection.connect();
            if (postBody != null) {
                OutputStream osPost = httpConnection.getOutputStream();
                osPost.write(postBody.getBytes());
                osPost.flush();
                osPost.close();
            }

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK || httpConnection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
                if (httpConnection.getHeaderField("Set-Cookie") != null) {
                    cookie_front = httpConnection.getHeaderField("Set-Cookie");
                    long cookies_timestamp = System.currentTimeMillis();
                }
                MessageDigest md5 = null;
                String md5String = httpConnection.getHeaderField("X-md5");
                if (md5String != null) md5 = MessageDigest.getInstance("MD5");

                InputStream in = httpConnection.getInputStream();
                if ("gzip".equalsIgnoreCase(httpConnection.getContentEncoding())) {
                    in = new GZIPInputStream(in);
                }
                if (os == null) os = new ByteArrayOutputStream();

                byte[] buffer = new byte[256];
                int bytesRead = 0, bytesTotal = 0;
                while ((bytesRead = in.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                    if (md5 != null) md5.update(buffer, 0, bytesRead);
                    bytesTotal += bytesRead;
                }

                if (md5 != null && !checkMD5(md5, md5String))
                    if (responseListener != null) {
                        responseListener.onError("Error in received data");
                    }
                if (os instanceof ByteArrayOutputStream) {
                    resultString = ((ByteArrayOutputStream) os).toString("UTF-8");
                }

            } else {
                if (responseListener != null) {
                    responseListener.onError("Server error: " + httpConnection.getResponseCode());
                }
            }

        } catch (MalformedURLException e) {
            if (responseListener != null) {
                responseListener.onError("Incorrect server URL : " + tUrl);
            }
        } catch (java.net.SocketTimeoutException e) {
            if (responseListener != null) {
                responseListener.onHTTPConnectionError("Server error (" + tUrl + ")" + " :" + e.toString());
            }
        } catch (IOException e) {
            if (responseListener != null) {
                responseListener.onHTTPConnectionError("Server error (" + tUrl + ")" + " :" + e.toString());
            }
        } catch (Exception e) {
            if (responseListener != null) {
                responseListener.onHTTPConnectionError("Server error (" + tUrl + ")" + " :" + e.toString());
            }
        } finally {
            if (httpConnection != null) {
                httpConnection.disconnect();
            }
        }
        if (responseListener != null) {
            responseListener.onResponse(resultString);
        }

    }


    public void sendGetOutData(final HTTP_CMD typeRequest, final String url, final TreeMap<String, String> headerData, final String requestBody, final OutputStream os, final ResponseListener responseListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                requestResponse(typeRequest, url, headerData, requestBody, os, responseListener);
            }
        }).start();

    }


    public interface ResponseListener {
        void onResponse(String response);

        public void onHTTPConnectionError(String error);

        public void onError(String error);
    }


    @SuppressLint("DefaultLocale")
    private static boolean checkMD5(MessageDigest tMd5, String md5String) {
        byte messageDigest[] = tMd5.digest();
        StringBuilder sb = new StringBuilder(2 * messageDigest.length);
        for (byte b : messageDigest) {
            sb.append(String.format("%02x", b & 0xff));
        }
        if (sb.toString().equals(md5String.toLowerCase())) {
            return true;
        }
        return false;
    }

    public void getWeatherForPosition(LatLng position, ResponseListener dataResponseListener) throws IOException {
        if (position == null || dataResponseListener == null) {
            throw new IOException("Illegal parametrs");
        }
        String url = DOMAIN_FRONT + "&lat=" + String.valueOf(position.latitude) + "&lon=" + String.valueOf(position.longitude);
        sendGetOutData(HTTP_CMD.GET, url, null, null, null, dataResponseListener);
    }
}
