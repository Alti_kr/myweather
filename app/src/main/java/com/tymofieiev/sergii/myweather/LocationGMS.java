package com.tymofieiev.sergii.myweather;


import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


/**
 * Created by TSV on 11.01.2017.
 */
class LocationGMS implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Context mContext;

    private MyOnLocationChangedListener listener;

    public LocationGMS(Context c, MyOnLocationChangedListener listener) {
        mContext = c;
        this.listener = listener;
    }

    public static final long LOCATION_FASTEST_INTERVAL = 1000L;
    public static final float LOCATION_DISPLACEMENT = 1f;

    public void getCurrentLocation() {
        long LOCATION_UPDATE_INTERVAL = 1000L;
        mLocationRequest = Utils.getLocationRequest(LOCATION_UPDATE_INTERVAL, LOCATION_FASTEST_INTERVAL, LOCATION_DISPLACEMENT);
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Starting the location updates
     */
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onConnectionFailed(null);
            }
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Stopping location updates
     */
    private void stopLocationUpdates() {
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        if (listener != null) {
            listener.onConnectionFailed(result);
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
    }

    @Override
    public void onLocationChanged(Location location) {
        Location curLocation = location;
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
        mGoogleApiClient = null;
        mLocationRequest = null;
        if (listener != null) {
            listener.onLocationChanged(curLocation);
        }
    }

    public interface MyOnLocationChangedListener {
        public void onConnectionFailed(ConnectionResult result);

        public void onLocationChanged(Location location);
    }
}