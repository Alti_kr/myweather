package com.tymofieiev.sergii.myweather;

import android.app.Application;
import android.content.Context;


public final class App extends Application {
    private static App instance;
    public App() {
        instance = this;
    }
    public static Context getContext() {
        return instance.getApplicationContext();
    }
}
