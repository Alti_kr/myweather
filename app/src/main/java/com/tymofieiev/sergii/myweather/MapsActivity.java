package com.tymofieiev.sergii.myweather;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.IOException;
import java.util.Date;

/**
 * Created by TSV on 11.01.2017.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {
    private Marker currentMarker;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public static final float LOCATION_DISPLACEMENT = 1f;
    public static long LOCATION_UPDATE_INTERVAL_FOR_ENABLE = 100L;
    public static final long LOCATION_FASTEST_INTERVAL_FOR_ENABLE = 100L;
    public static int REQUEST_ON_GPS = 2222;
    private Location currentLocation;
    private ListView listView;
    private PlaceAutocompleteAdapter mAdapter;
    public static float ZOOM_LEVEL_NORMAL = 8.0f;
    private SearchView searchView;
    private WeatherPlace currentWeatherPlace;
    private DisplayImageOptions options;
    private SharedPreferences sPref;
    private String SAVED_LAT = "SAVED_LAT";
    private String SAVED_LNG = "SAVED_LNG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        searchView = (SearchView) findViewById(R.id.search_place);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText.toLowerCase());
                if (newText.trim().isEmpty()) {
                    clearSearchList();
                    listView.setVisibility(View.GONE);
                } else {
                    if (listView.getVisibility() != View.VISIBLE) {
                        listView.setVisibility(View.VISIBLE);
                    }
                }
                return false;
            }
        });
        listView = (ListView) findViewById(R.id.search_list_view);
        listView.setVisibility(View.GONE);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        onConnectedApiClient();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .build();

        mAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, Utils.getDefaultBounds(), null);
        listView.setOnItemClickListener(mAutocompleteClickListener);
        listView.setAdapter(mAdapter);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        if (isGoogleApiAvailable()) {
            tryGetGPS(false);
        }

        currentLocation = getSavedLocation();
        if(!Utils.isInternetAvailable(this)){
            Toast.makeText(MapsActivity.this, Utils.getStringById(R.string.exc_no_network), Toast.LENGTH_SHORT).show();
            Utils.setMobileDataTransferEnabled(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    private void clearSearchList() {
        if (mAdapter != null) {
            mAdapter.clearList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.initImageLoader(this);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ON_GPS) {
            if (resultCode == RESULT_OK) {
                tryGetGPS(true);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void tryGetGPS(boolean is) {
        if (Utils.isLocationEnabled(this)) {
            LocationGMS locationGMS = new LocationGMS(this, new LocationGMS.MyOnLocationChangedListener() {
                @Override
                public void onConnectionFailed(ConnectionResult result) {
                    Toast.makeText(MapsActivity.this, result.toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLocationChanged(Location location) {
                    saveLastLocation(location);

                }
            });
            locationGMS.getCurrentLocation();
        } else {
            if (is) {
                onConnectedApiClient();
            }
        }
    }

    private void saveLastLocation(Location location) {
        currentLocation = location;
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(SAVED_LAT, String.valueOf(currentLocation.getLatitude()));
        ed.putString(SAVED_LNG, String.valueOf(currentLocation.getLongitude()));
        ed.commit();
    }

    private Location getSavedLocation() {
        sPref = getPreferences(MODE_PRIVATE);

        LatLng latLng = Utils.makeLatLng(sPref.getString(SAVED_LAT, ""), sPref.getString(SAVED_LNG, ""));
        Location savedLocation = new Location("");
        savedLocation.setLatitude(latLng.latitude);
        savedLocation.setLongitude(latLng.longitude);
        savedLocation.setTime(new Date().getTime());
        return savedLocation;
    }

    private void onConnectedApiClient() {
        final Activity mContext = this;
        LocationRequest locationRequest = Utils.getLocationRequest(LOCATION_UPDATE_INTERVAL_FOR_ENABLE, LOCATION_FASTEST_INTERVAL_FOR_ENABLE, LOCATION_DISPLACEMENT);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                        try {

                            status.startResolutionForResult(
                                    mContext, REQUEST_ON_GPS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng initPosition = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(initPosition));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MapsActivity.this, Utils.getStringById(R.string.exc_map), Toast.LENGTH_SHORT).show();
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                processMyPosition();
                return false;
            }
        });
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                processOnLongClick(latLng);
            }
        });
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        processOnLongClick(initPosition);
    }

    private void processMyPosition() {
        if (currentLocation != null) {
            processOnLongClick(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
        } else {
            if (isGoogleApiAvailable()) {
                tryGetGPS(true);
            }
        }

    }

    private void processOnLongClick(LatLng position) {
        if (currentMarker != null) {
            currentMarker.remove();
        }
        currentMarker = null;
        if (mMap != null) {
            currentMarker = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        }
        moveMapTo(position);
        getDataForLatLng(position);
    }

    private void getDataForLatLng(LatLng position) {
        Communication communication = Communication.getInstance();
        try {
            communication.getWeatherForPosition(position, dataResponseListener);
        } catch (IOException e) {
            Toast.makeText(MapsActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private Communication.ResponseListener dataResponseListener = new Communication.ResponseListener() {
        @Override
        public void onResponse(String response) {
            workHandler.sendMessage(getHandlerMessageMultiParam(1, new String[]{response}));
        }

        @Override
        public void onHTTPConnectionError(String error) {
            workHandler.sendMessage(getHandlerMessageMultiParam(2, new String[]{error}));
        }

        @Override
        public void onError(String error) {
            workHandler.sendMessage(getHandlerMessageMultiParam(2, new String[]{error}));
        }
    };

    private void showInfo(String response) {
        currentWeatherPlace = new WeatherPlace(response);
        currentMarker.showInfoWindow();
    }

    private void error(String error) {
        Toast.makeText(MapsActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    private Handler workHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                showInfo(msg.getData().getStringArray(HM_QUERY_PARAMETERS_0)[0]);
            } else if (msg.what == 2) {
                error(msg.getData().getStringArray(HM_QUERY_PARAMETERS_0)[0]);
            }
        }
    };

    public static String HM_QUERY_PARAMETERS_0 = "HM_QUERY_PARAMETERS_0";

    public static Message getHandlerMessageMultiParam(int what, String[] params) {
        Bundle ban = new Bundle();
        ban.putStringArray(HM_QUERY_PARAMETERS_0, params);
        Message mm = new Message();
        mm.what = what;
        mm.setData(ban);
        return mm;
    }

    private boolean isGoogleApiAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        if (resultCode != ConnectionResult.SUCCESS) {
            Dialog dialog = googleAPI.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            if (dialog != null) {
                dialog.show();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(MapsActivity.this, R.string.error_google_api_client, Toast.LENGTH_SHORT).show();
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            final Place place = places.get(0);
            searchView.setQuery("", false);
            searchView.clearFocus();
            processOnLongClick(place.getLatLng());
            places.release();
        }
    };

    private void moveMapTo(LatLng latLng) {
        if (latLng != null && mMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom < 2.1 ? ZOOM_LEVEL_NORMAL : mMap.getCameraPosition().zoom);

            /*if (isNeedMoveStartPosition) {
                mMap.moveCamera(cameraUpdate);
            } else {
                mMap.animateCamera(cameraUpdate);
            }*/
            mMap.animateCamera(cameraUpdate);

        }
    }



    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private View view;

        MyInfoWindowAdapter() {
            view = getLayoutInflater().inflate(R.layout.info_view, null);
        }

        @Override
        public View getInfoContents(Marker marker) {
            if (currentMarker != null && currentMarker.isInfoWindowShown()) {
                currentMarker.hideInfoWindow();
                currentMarker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(Marker arg0) {
            if (currentWeatherPlace != null) {
                if (currentWeatherPlace.getImageName() == null || currentWeatherPlace.getImageName().isEmpty()) {
                    ((ImageView) view.findViewById(R.id.w_image)).setImageResource(R.drawable.weather_logo);
                } else {
                    ImageLoader.getInstance().displayImage(currentWeatherPlace.getImageName(), ((ImageView) view.findViewById(R.id.w_image)), options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view,
                                                      Bitmap loadedImage) {
                            super.onLoadingComplete(imageUri, view, loadedImage);
                            getInfoContents(currentMarker);
                        }
                    });
                }
                ((TextView) view.findViewById(R.id.w_header)).setText(String.format(Utils.getStringById(R.string.w_title), currentWeatherPlace.getPlaceName(), currentWeatherPlace.getPlaceCode()));
                ((TextView) view.findViewById(R.id.w_temp)).setText(String.format(Utils.getStringById(R.string.w_temperature), currentWeatherPlace.getTemperature()));
            }
            return view;
        }
    }
}
